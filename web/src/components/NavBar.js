import React from 'react';
import Notification from './Notification';

export default function NavBar() {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row-reverse',
        width: '100%',
        height: '80px',
        background: '#7159C1',
      }}
    >
      <Notification />
    </div>
  );
}
