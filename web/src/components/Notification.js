import React, { useState, useMemo, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { FaBell } from 'react-icons/fa';
import socketio from 'socket.io-client';

export default function Notification() {
  const [anchorEl, setAnchorEl] = useState(null);
  const [notifications, setNotifications] = useState([]);
  const user = { id: 1 };

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const socket = useMemo(
    () =>
      socketio('http://localhost:3333', {
        query: {
          user_id: user.id,
        },
      }),
    [user.id]
  );

  useEffect(() => {
    socket.on('notification', notification => {
      setNotifications([notification, ...notifications]);
    });
  }, [socket, notifications]);

  return (
    <>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
        style={{
          marginRight: '15px',
        }}
      >
        <FaBell style={{ color: '#fff' }} size={28} />
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {notifications.map(notification => (
          <MenuItem key={notification} onClick={handleClose}>
            {notification.message}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
}
