import { Router } from 'express';
import Notification from './app/Notification';

const routes = new Router();

routes.post('/', Notification.store);

export default routes;
