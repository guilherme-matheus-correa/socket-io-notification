class Notification {
  async store(req, res) {
    // Pega os usuarios e o socket do middleware
    const { connectedUsers, io } = req;
    const { user_id } = req.body;

    const notification = { message: 'Nova notificação' };

    const connectedUser = connectedUsers[user_id];

    // Se o usuario da sessão estiver conectado envia notificação
    if (connectedUser) {
      io.to(connectedUser).emit('notification', notification);
    }

    res.status(200).json();
  }
}

export default new Notification();
