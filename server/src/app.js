import 'dotenv/config';

import Youch from 'youch';
import express from 'express';
import 'express-async-errors';

import SocketIO from 'socket.io';
import http from 'http';
import routes from './routes';

class App {
  constructor() {
    this.app = express();
    this.server = http.Server(this.app);

    this.socket();
    this.middlewares();
    this.routes();
    this.exceptionHandler();

    this.connectedUsers = [];
  }

  socket() {
    this.socketIO = SocketIO(this.server);
    this.socketIO.on('connection', socket => {
      console.log('User connected');
      const { user_id } = socket.handshake.query;
      this.connectedUsers[user_id] = socket.id;

      socket.on('disconnected', () => {
        console.log('User disconnected');
        delete this.connectedUsers[user_id];
      });
    });
  }

  middlewares() {
    this.app.use(express.json());
    // SocketIO middleware
    this.app.use((req, res, next) => {
      req.io = this.socketIO;
      req.connectedUsers = this.connectedUsers;

      next();
    });
  }

  routes() {
    this.app.use(routes);
  }

  exceptionHandler() {
    this.app.use(async (err, req, res, next) => {
      if (process.env.NODE_ENV === 'development') {
        const errors = await new Youch(err, req).toJSON();

        return res.status(500).json(errors);
      }

      return res.status(500).json({ error: 'Internal server error' });
    });
  }
}

export default new App().server;
